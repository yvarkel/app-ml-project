import datetime
import torch.nn as nn
import torch.utils.data
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from utils import calc_loss, create_if_not_exist, try_cuda


def train(model, criterion, optimizer, train_loader, test_loader, num_epochs, is_cuda, output_files_prefix,
          model_name, checkpoint_epoch_interval=1, plot_epoch_interval=1, train_no_augment_loader=None,
          evaluation_criterion=None):

    if train_no_augment_loader is None:
        train_no_augment_loader = train_loader

    if evaluation_criterion is None:
        evaluation_criterion = criterion

    train_losses, test_losses = [], []
    # create output dir if doesn't exist
    create_if_not_exist('output')
    with open('output/{}_losses.txt'.format(output_files_prefix), 'a') as f:
        f.write('------ Starting To Train ------\n')
    for epoch in range(1, num_epochs + 1):
        start = datetime.datetime.now()
        for i, (x, y) in enumerate(train_loader, start=1):
            x, y = try_cuda(x, y)

            # Forward pass
            prediction = model(x)
            loss = criterion(prediction, y)

            # Backprop and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if i % 10 == 0:
                print("Epoch[{}/{}], Step [{}/{}], Loss: {}"
                      .format(epoch, num_epochs, i, len(train_loader), loss))

        end = datetime.datetime.now()
        print ("Epoch train duration: {}".format(end - start))

        train_loss = calc_loss(model, evaluation_criterion, train_no_augment_loader)
        train_losses.append(train_loss)
        test_loss = calc_loss(model, evaluation_criterion, test_loader)
        test_losses.append(test_loss)

        epoch_stats = "Epoch[{}/{}], Train loss: {}, Test loss: {}".format(
            epoch, num_epochs, train_loss, test_loss)
        print (epoch_stats)

        with open('output/{}_losses.txt'.format(output_files_prefix), 'a') as f:
            f.write(epoch_stats + '\n')

        if epoch % checkpoint_epoch_interval == 0 or epoch == num_epochs:
            model_path = 'output/{}_epoch_{}.pt'.format(model_name, epoch)
            print ("Saving checkpoint to " + model_path)
            torch.save(model.state_dict(), model_path)

        if epoch % plot_epoch_interval == 0 or epoch == num_epochs:
            # Plot cost
            plt.plot(train_losses, color='blue')
            plt.plot(test_losses, color='green')
            plt.legend(['Train loss', 'Test loss'])
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.savefig('output/{}_losses_epoch_{}.png'.format(output_files_prefix, epoch))

