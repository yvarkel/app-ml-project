import pandas as pd
from preprocess import preprocess
from sklearn.ensemble import RandomForestRegressor
import numpy as np


def RMSE(y, y_pred):
    return np.mean((y - y_pred) ** 2) ** 0.5

# load data
train = pd.read_table('../data/train.tsv')

X_train, X_valid, y_train, y_valid = preprocess(train)

# apply log1p since sklearn random forest doesn't have RMSLE loss
y_train = np.log1p(y_train)
y_valid = np.log1p(y_valid)


rf = RandomForestRegressor()
rf = RandomForestRegressor(20, criterion='mse', max_depth=50, verbose=2, n_jobs=-1)
rf.fit(X_train, y_train)

print("RMSLE train", RMSE(y_train, rf.predict(X_train)))
print("RMSLE validation", RMSE(y_valid, rf.predict(X_valid)))


