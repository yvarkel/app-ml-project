import scipy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder


def preprocess(train):
    # columns are: 'train_id', 'name', 'item_condition_id', 'category_name', 'brand_name', 'price', 'shipping', 'item_description'

    # fill in nan
    # item_description has only 4 NaNs, just drop them
    train.dropna(subset=['item_description'], inplace=True)

    # category_name, brand_name have many NaNs, fill with empty string so later one hot encoder will encode those.
    train.fillna('', inplace=True)

    # split to train and validation
    X_train, X_valid, y_train, y_valid = train_test_split(train.drop(columns='price'),
                                                          train['price'].values.astype('float32'), test_size=0.1,
                                                          random_state=0)

    one_hot_encoder = OneHotEncoder(handle_unknown='ignore')
    name_vectorizer = TfidfVectorizer(max_features=120000, ngram_range=[1, 2])
    description_vectorizer = TfidfVectorizer(max_features=120000, ngram_range=[1, 2])

    X_train = scipy.sparse.hstack([
        one_hot_encoder.fit_transform(
            X_train.filter(items=['item_condition_id', 'category_name', 'brand_name', 'shipping'])),
        name_vectorizer.fit_transform(X_train['name']),
        description_vectorizer.fit_transform(X_train['item_description'])
    ])

    X_valid = scipy.sparse.hstack([
        one_hot_encoder.transform(
            X_valid.filter(items=['item_condition_id', 'category_name', 'brand_name', 'shipping'])),
        name_vectorizer.transform(X_valid['name']),
        description_vectorizer.transform(X_valid['item_description'])
    ])

    return X_train, X_valid, y_train, y_valid