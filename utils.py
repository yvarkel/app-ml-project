import collections
import os
import numpy as np
import torch
import torch.utils.data

is_cuda = torch.cuda.is_available()


def try_cuda(x, y):
    if is_cuda:
        y = y.cuda()
        if isinstance(x, torch.Tensor):
            x = x.cuda()
        elif isinstance(x, collections.Sequence):
            x = [it.cuda() for it in x]
        else:
            raise TypeError('x should be tensor or sequence of tensors')

    return x, y


def calc_loss(model, criterion, loader):
    outputs = []
    ys = []
    with torch.no_grad():
        # Calculate cost
        for (x, y) in loader:
            x, y = try_cuda(x, y)

            output = model(x)
            outputs.append(output)
            ys.append(y)

        outputs = torch.cat(outputs)
        ys = torch.cat(ys)
        loss = criterion(outputs, ys)
        return loss.item()


def calc_loss_batching(model, criterion, loader):
    batch_cost = []
    with torch.no_grad():
        # Calculate cost
        for (x, y) in loader:
            x, y = try_cuda(x, y)

            outputs = model(x)
            loss = criterion(outputs, y)
            batch_cost.append(loss.item())

        return np.mean(batch_cost)


def create_if_not_exist(dir='output'):
    if not os.path.isdir(dir):
        os.mkdir(dir)

