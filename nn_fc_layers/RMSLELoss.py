import torch.nn as nn


class RMSLELoss(nn.MSELoss):

    def __init__(self, size_average=None, reduce=None, reduction='elementwise_mean'):
        super(RMSLELoss, self).__init__(size_average, reduce, reduction)

    def forward(self, input, target):
        return super(RMSLELoss, self).forward(input.log1p(), target.log1p()).sqrt()
