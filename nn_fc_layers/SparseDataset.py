from torch.utils.data import Dataset
import torch
import numpy as np
import torch.sparse

class SparseDataset(Dataset):

    def __init__(self, matrix, ys, transform=None):
        self.matrix = matrix.tocsr()
        self.ys = ys
        self.transform = transform

    def __len__(self):
        return len(self.ys)

    def __getitem__(self, index):
        x = self.matrix[index, :]
        x = x.todense()
        x = torch.FloatTensor(x)

        if self.transform is not None:
            x = self.transform(x)

        return x, self.ys[index]



