import torch
from torch import nn as nn


class FC(nn.Module):
    def __init__(self, input_features_count):
        super(FC, self).__init__()
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(input_features_count, 500),
            torch.nn.ReLU(),
            torch.nn.Linear(500, 500),
            torch.nn.ReLU(),
            torch.nn.Linear(500, 1),
            torch.nn.ReLU())

    def forward(self, x):
        x = self.layers(x)
        x = x.view(x.size(0))
        return x

if __name__ == "__main__":
    from torchsummary import summary

    input_features_count = 245959
    model = FC(input_features_count)
    summary(model, (1, input_features_count))
