import pandas as pd
import torch
from torch.optim import Adam
from torch.utils.data import DataLoader
# from torchsummary import summary

import train_utils
from FC import FC
from RMSLELoss import RMSLELoss
from SparseDataset import SparseDataset
from preprocess import preprocess

is_cuda = torch.cuda.is_available()

# load data
train = pd.read_table('../data/train.tsv')

X_train, X_valid, y_train, y_valid = preprocess(train)

# build model
features_count = X_train.shape[1]
model = FC(features_count)
if is_cuda:
  model = model.cuda()
# summary(model, (1, features_count))

# train
train_loader = DataLoader(SparseDataset(X_train, y_train), batch_size=1024, num_workers=5)
validation_loader = DataLoader(SparseDataset(X_valid, y_valid), batch_size=1024, num_workers=5)
criterion = RMSLELoss()
optimizer = Adam(model.parameters())
train_utils.train(model, criterion, optimizer, train_loader, validation_loader, 5, is_cuda, 'fc', 'fc')
