------ Starting To Train ------
Epoch[1/20], Train loss: 0.456557124853, Test loss: 0.477317452431
Epoch[2/20], Train loss: 0.418331474066, Test loss: 0.449622303247
Epoch[3/20], Train loss: 0.408171236515, Test loss: 0.447150826454
Epoch[4/20], Train loss: 0.399788171053, Test loss: 0.443652451038
Epoch[5/20], Train loss: 0.393928855658, Test loss: 0.44123545289
Epoch[6/20], Train loss: 0.389025866985, Test loss: 0.440565526485
Epoch[7/20], Train loss: 0.384697645903, Test loss: 0.439356297255
Epoch[8/20], Train loss: 0.382934868336, Test loss: 0.438877403736
Epoch[9/20], Train loss: 0.379761695862, Test loss: 0.438747018576
Epoch[10/20], Train loss: 0.379374474287, Test loss: 0.439717710018
