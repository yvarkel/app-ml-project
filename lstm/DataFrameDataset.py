from torch.utils.data import Dataset
import torch
import numpy as np
import torch.sparse

class DataFrameDataset(Dataset):

    def __init__(self, df, ys):
        df = df.reset_index()
        self.texts = df['tokens_text']
        self.numerics = df.drop(columns='tokens_text')
        self.ys = ys

    def __len__(self):
        return len(self.ys)

    def __getitem__(self, index):
        texts = self.texts[index]
        numerics = self.numerics.iloc[index]

        texts = torch.LongTensor(texts)
        numerics = torch.FloatTensor(numerics)

        return (texts, numerics), self.ys[index]



