import numpy as np
import pandas as pd
import torch
import torch.nn.functional as F
from torch import nn
from torch import optim
from torch.utils.data import DataLoader

import train_utils
from lstm.DataFrameDataset import DataFrameDataset
from lstm.LstmModel import LstmModel
from lstm.Vocab import Vocab

is_cuda = torch.cuda.is_available()

X_train = pd.read_pickle('../data/x_train.gz')
X_valid = pd.read_pickle('../data/x_valid.gz')
y_train = np.load('../data/y_train.npy')
y_valid = np.load('../data/y_valid.npy')

# X_train = X_train[0:5]
# X_valid = X_valid[0:5]
# y_train = y_train[0:5]
# y_valid = y_valid[0:5]

# fit vocab on train and transform
vocab = Vocab()
for column in ['text']:
    X_train['tokens_' + column] = X_train['tokens_' + column].apply(lambda x: vocab.fit_transform_document(x))

# transform validation using vocab
for column in ['text']:
    X_valid['tokens_' + column] = X_valid['tokens_' + column].apply(lambda x: vocab.transform_document(x))

print (vocab.word_count, "words in vocabulary")

y_train = np.log1p(y_train)
y_valid = np.log1p(y_valid)

y_mean = np.mean(y_train)
y_std = np.std(y_train)
y_train = (y_train - y_mean) / y_std
y_valid = (y_valid - y_mean) / y_std
y_std = y_std.item()

n_layers = 2
hidden_size = 800
embedding_size = 300

model = LstmModel(vocab.word_count, hidden_size, embedding_size, n_layers)
if is_cuda:
  model = model.cuda()

criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=1e-4)

# create dataloaders
train_loader = DataLoader(DataFrameDataset(X_train, y_train), batch_size=1, num_workers=8)
valid_loader = DataLoader(DataFrameDataset(X_valid, y_valid), batch_size=1, num_workers=8)

train_utils.train(model, criterion, optimizer, train_loader, valid_loader, 20, is_cuda, 'lstm', 'lstm',
                  checkpoint_epoch_interval=1000,
                  evaluation_criterion=lambda pred, y: F.mse_loss(pred * y_std, y * y_std).sqrt())
