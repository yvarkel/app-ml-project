import torch
from torch import nn
import torch.nn.functional as F

from lstm.Attn import Attn

is_cuda = torch.cuda.is_available()
device = torch.device("cuda" if is_cuda else "cpu")

class LstmModel(nn.Module):
    def __init__(self, input_size, hidden_size, embedding_size, n_layers, numerics_size = 8):
        super(LstmModel, self).__init__()
        self.hidden_size = hidden_size
        self.n_layers = n_layers

        self.embedding = nn.Embedding(input_size, embedding_size)

        self.lstm = nn.LSTM(embedding_size, hidden_size, n_layers, bidirectional=True)
        self.linear_for_numerics = nn.Linear(numerics_size, numerics_size)
        self.out = nn.Sequential(
            nn.Linear(hidden_size * 2 + numerics_size, 200),
            nn.ReLU(),
            nn.Linear(200, 64),
            nn.ReLU(),
            nn.Linear(64, 1)
        )
        self.attn = Attn(hidden_size)

    def forward(self, inputs):
        text, numerics = inputs
        seq_len = text.shape[1]

        embedded_words = self.embedding(text).view(seq_len, 1, -1)

        last_hidden = self.init_hidden()
        rnn_outputs, hidden = self.lstm(embedded_words, last_hidden)
        attn_weights = self.attn(rnn_outputs)
        attn_weights = attn_weights.squeeze(1).view(seq_len, 1)
        rnn_outputs = rnn_outputs.squeeze(1)
        attn_weights = attn_weights.expand(seq_len, self.hidden_size*2)
        weigthed_outputs = torch.mul(rnn_outputs, attn_weights)
        output = torch.sum(weigthed_outputs, -2)
        numerics = F.relu(self.linear_for_numerics(numerics))
        output = torch.cat([output, numerics.squeeze(0)])

        output = self.out(output)
        return output

    def init_hidden(self):
        return (torch.zeros(self.n_layers*2, 1, self.hidden_size).to(device),
                    torch.zeros(self.n_layers*2, 1, self.hidden_size).to(device))