class Vocab:
    def __init__(self):
        self.word_count = 1
        self.ind2word = {0: 'UNKNOWN_WORD'}
        self.word2ind = {'UNKNOWN_WORD': 0}

    def fit_transform_document(self, document):
        indexes = []
        for token in document:
          indexes.append(self.fit_transform_token(token))
        return indexes

    def transform_document(self, document):
        indexes = []
        for token in document:
            indexes.append(self.transform_token(token))
        return indexes

    def fit_transform_token(self, token):
        if token not in self.word2ind:
            self.word2ind[token] = self.word_count
            self.ind2word[self.word_count] = token
            self.word_count += 1
        return self.word2ind[token]

    def transform_token(self, token):
        if token not in self.word2ind:
            return 0
        return self.word2ind[token]
