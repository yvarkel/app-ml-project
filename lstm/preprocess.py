import time

import numpy as np
import pandas as pd
import spacy as sp
import torch
from sklearn.model_selection import train_test_split

from lstm.Vocab import Vocab

is_cuda = torch.cuda.is_available()


data = pd.read_table('../data/train.tsv')

# fill in nan
# item_description has only 4 NaNs, just drop them
data.dropna(subset=['item_description'], inplace=True)

# category_name, brand_name have many NaNs
data.fillna({'category_name': 'NaN_Category', 'brand_name': 'NaN_Brand'}, inplace=True)

# one hot encode
categorial_columns = ['item_condition_id', 'shipping']
data = pd.get_dummies(data, columns=categorial_columns)

data.drop(columns='train_id', inplace=True)

data['text'] = data['brand_name'] + ' ' + data['name'] + ' ' + data['category_name'].str.replace('/', ' ') + ' ' + data['item_description']
data.drop(columns=['item_description', 'name', 'category_name', 'brand_name'], inplace=True)

# tokenize
nlp = sp.load('en', disable=['parser', 'ner']) # run python -m spacy download en
start = time.time()
for column in ['text']:
    data['tokens_' + column] = data[column].apply(lambda x: [t.lemma_ for t in nlp(unicode(x.strip().lower(), 'latin-1'))])
    data.drop(columns=[column], inplace=True)
end = time.time()
print ('Lemmatization took:', end - start)

# split to train and validation
X_train, X_valid, y_train, y_valid = train_test_split(data.drop(columns='price'),
                                                      data['price'].values.astype('float32'), test_size=0.1,
                                                      random_state=0)
X_train.to_pickle('../data/x_train.gz')
X_valid.to_pickle('../data/x_valid.gz')
np.save('../data/y_train.npy', y_train)
np.save('../data/y_valid.npy', y_valid)
